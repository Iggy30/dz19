﻿#include <iostream>
using namespace std;

class Animal
{
public:
  
   virtual void Voice()
    {
       cout << "";
    };
     
   virtual ~Animal() = 0;
   

};

Animal::~Animal() {};

class Dog :public Animal
{
public:
    
    void Voice() override
    {
        cout << "Woof!"<<'\n';
    }

    ~Dog()  override {};
   
};

class Cat :public Animal
{
public:
    
    void Voice() override
    {
        cout << "Mrrr"<<'\n';
    }

    ~Cat() override {};
};

class Cow :public Animal
{
public:
    
    void Voice() override
    {
        cout << "Moooo"<<'\n';
    }

    ~Cow() override {};
};

class Dack :public Animal
{
public:
     
    void Voice()
    {
        cout << "Quack" << '\n';
    }

    ~Dack() override {}
  
};

int main()
{
    Animal* animalsvoice[4];

    animalsvoice[0] = new Dog();
    animalsvoice[1] = new Cat();
    animalsvoice[2] = new Cow();
    animalsvoice[3] = new Dack();
    
    
    for (Animal* a : animalsvoice)
    {
        a->Voice();
        delete a;

    }
   //delete animalsvoice[0]; delete animalsvoice[1]; delete animalsvoice[2]; delete animalsvoice[3];
    
};


